package com.gdmob.jweb.model;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Table;
import com.gdmob.jweb.config.Constants;
import com.gdmob.jweb.plugin.PropertiesPlugin;
import com.gdmob.jweb.tools.ToolDateTime;

@SuppressWarnings("unused")
@Table(tableName = "jw_upload")
public class Upload extends BaseModel<Upload> {

	private static final long serialVersionUID = 2051998642258015518L;

	private static Logger log = Logger.getLogger(Upload.class);

	public static final Upload dao = new Upload();

	public void deleteAll(String ids) {
		try{
			if(StringUtils.isNotBlank(ids)){
				String dir = (String) PropertiesPlugin.getParamMapValue(Constants.DATA_DIR)+File.separator;
				Upload upload = dao.findById(ids,"path");
				String path = upload.getStr("path");
				String fullPath = dir + path;
				FileUtils.forceDelete(new File(fullPath));
				dao.deleteById(ids);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public String copy(Upload upload,String type){
		try{
			Upload newUpload = new Upload();
			newUpload.set("type",type);
			newUpload.set("contenttype", upload.get("contenttype"));
			newUpload.set("originalfilename",upload.get("originalfilename"));
			String dir = (String) PropertiesPlugin.getParamMapValue(Constants.DATA_DIR);
			File sourceFile = new File(dir + File.separator + upload.getStr("path"));
			String typeDir = type+File.separator+ToolDateTime.getCurrentYmd();
			String destDir = (String) PropertiesPlugin.getParamMapValue(Constants.DATA_DIR)+File.separator+typeDir;
			String sourceFileName = upload.getStr("getFileName");
			File destFile = new File(destDir,sourceFileName);
				FileUtils.moveFile(sourceFile,destFile);
			String filePath = typeDir + File.separator + sourceFileName;
			filePath.replace("\\","/");
			newUpload.set("path",filePath);
			newUpload.save();
			String ids = newUpload.getStr("ids");
			return ids;
		}catch(Exception e){
			return null;
		}
	}
}
