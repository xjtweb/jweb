package com.gdmob.jweb.model;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Table;

@SuppressWarnings("unused")
@Table(tableName="jw_region")
public class Region extends BaseModel<Region> {
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(Region.class);
	
	public static final Region dao = new Region();
}
