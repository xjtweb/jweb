package com.gdmob.jweb.model;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.gdmob.jweb.tools.ToolDateTime;
import com.gdmob.jweb.tools.ToolSqlXml;
import com.gdmob.jweb.tools.ToolUtils;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Table;
import com.jfinal.plugin.activerecord.TableMapping;

/**
 * Model基础类
 * @param <M>
 */
public abstract class BaseModel<M extends Model<M>> extends Model<M> {

	private static final long serialVersionUID = -900378319414539856L;

	private static Logger log = Logger.getLogger(BaseModel.class);
	
	/**
	 * 根据i18n参数查询获取哪个字段的值
	 * @param i18n
	 * @return
	 */
	public static String i18n(String i18n){
		String val = null;
		if(i18n.equals("zh") || i18n.equals("zh_cn")){
			val = "_zhcn";
			
		} else if(i18n.equals("en") || i18n.equals("en_us")){
			val = "_enus";
			
		} else if(i18n.equals("zh_tw")){
			val = "_zhtw";
			
		} else {
			val = "_zhcn";
		}
		return val;
	}
	
	/**
	 * 获取表映射对象
	 * 
	 * @return
	 */
	public Table getTable() {
		return TableMapping.me().getTable(getClass());
	}

	/**
	 * 获取主键值
	 * @return
	 */
	public String getPrimaryKeyValue(){
		return this.getStr(getTable().getPrimaryKey());
	}

	/**
	 * 重写save方法
	 */
	public boolean save() {
		this.set(getTable().getPrimaryKey(), ToolUtils.getUuidByJdk(true)); // 设置主键值
		if(getTable().hasColumnLabel("version")){ // 是否需要乐观锁控制
			this.set("version", Long.valueOf(0)); // 初始化乐观锁版本号
		}
		String currentTime = ToolDateTime.getCurrentDate();
		if(getTable().hasColumnLabel("createtime")&&StringUtils.isBlank(this.getStr("createtime"))){
			this.set("createtime",currentTime);
		}
		if(getTable().hasColumnLabel("modifytime")&&StringUtils.isBlank(this.getStr("modifytime"))){
			this.set("modifytime",currentTime);
		}
		return super.save();
	}
	public void saveWithDbKey() {
		if(getTable().hasColumnLabel("version")){ // 是否需要乐观锁控制
			this.set("version", Long.valueOf(0)); // 初始化乐观锁版本号
		}
		String currentTime = ToolDateTime.getCurrentDate();
		if(getTable().hasColumnLabel("createtime")&&StringUtils.isBlank(this.getStr("createtime"))){
			this.set("createtime",currentTime);
		}
		if(getTable().hasColumnLabel("modifytime")&&StringUtils.isBlank(this.getStr("modifytime"))){
			this.set("modifytime",currentTime);
		}
		super.save();
	}
	/**
	 * 重写update方法
	 */
	@SuppressWarnings("unchecked")
	public boolean update() {
		if(getTable().hasColumnLabel("version")){
			Table table = getTable();
			String name = table.getName();
			String pk = table.getPrimaryKey();
			// 1.数据是否还存在
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("table", name);
			param.put("pk", pk);
			String sql = ToolSqlXml.getSql("pingtai.baseModel.version", param); 
			Model<M> modelOld = findFirst(sql , getStr("ids"));
			if(null == modelOld){ // 数据已经被删除
				throw new RuntimeException("数据库中此数据不存在，可能数据已经被删除，请刷新数据后在操作");
			}
			
			// 2.乐观锁控制
			Set<String> modifyFlag = null;
			try {
				Field field = this.getClass().getSuperclass().getSuperclass().getDeclaredField("modifyFlag");
				field.setAccessible(true);
				Object object = field.get(this);
				if(null != object){
					modifyFlag = (Set<String>) object;
				}
				field.setAccessible(false);
			} catch (NoSuchFieldException | SecurityException e) {
				log.error("业务Model类必须继承BaseModel");
				e.printStackTrace();
				throw new RuntimeException("业务Model类必须继承BaseModel");
			} catch (IllegalArgumentException | IllegalAccessException e) {
				log.error("BaseModel访问modifyFlag异常");
				e.printStackTrace();
				throw new RuntimeException("BaseModel访问modifyFlag异常");
			}
			boolean versionModify = modifyFlag.contains("version");
			if(versionModify && getTable().hasColumnLabel("version")){ // 是否需要乐观锁控制
				Long versionDB = modelOld.getNumber("version").longValue(); // 数据库中的版本号
				Long versionForm = getNumber("version").longValue(); // 表单中的版本号
				if(!(versionForm > versionDB)){
					throw new RuntimeException("表单数据版本号和数据库数据版本号不一致，可能数据已经被其他人修改，请重新编辑");
				}
			}
		}
		String currentTime = ToolDateTime.getCurrentDate();
		if(getTable().hasColumnLabel("modifytime")&&StringUtils.isBlank(this.getStr("modifytime"))){
			this.set("modifytime",currentTime);
		}
		return super.update();
	}
	public boolean saveModel(){
		this.set(getTable().getPrimaryKey(), ToolUtils.getUuidByJdk(true)); // 设置主键值
		String currentTime = ToolDateTime.getCurrentDate();
		if(getTable().hasColumnLabel("createtime")&&StringUtils.isBlank(this.getStr("createtime"))){
			this.set("createtime",currentTime);
		}
		if(getTable().hasColumnLabel("modifytime")&&StringUtils.isBlank(this.getStr("modifytime"))){
			this.set("modifytime",currentTime);
		}
		return super.save();
	}
	public boolean updateModel(){
		String currentTime = ToolDateTime.getCurrentDate();
		if(getTable().hasColumnLabel("modifytime")&&StringUtils.isBlank(this.getStr("modifytime"))){
			this.set("modifytime",currentTime);
		}
		return super.update();
	}
}
