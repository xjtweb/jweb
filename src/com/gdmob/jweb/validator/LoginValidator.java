package com.gdmob.jweb.validator;

import org.apache.log4j.Logger;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class LoginValidator extends Validator {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(LoginValidator.class);
	
	protected void validate(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/jw/login/vali")){
			validateString("username", 5, 16, "usernameMsg", "用户名为5-16位!");
			validateString("password", 6, 18, "passwordMsg", "密码为6-18位!");
			validateRequired("authCode", "authCodeMsg", "请输入验证码！");
		}
	}
	
	protected void handleError(Controller controller) {
		String actionKey = getActionKey();
		if (actionKey.equals("/jw/login/vali")){
			controller.keepPara("username", "password","authCode");
			controller.render("/pingtai/login.html");
		}
	}
	
}
