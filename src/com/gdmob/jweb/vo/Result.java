package com.gdmob.jweb.vo;

public class Result {
	public static final int SUCCESS = 1;
	public static final int FAILURE = 0;
	public static final String STATUS = "status";
	public static final String ERROR = "error";
}
