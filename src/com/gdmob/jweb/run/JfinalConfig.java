package com.gdmob.jweb.run;

import java.io.File;

import org.apache.log4j.Logger;
import org.beetl.core.GroupTemplate;

import com.gdmob.jweb.beetl.format.DateFormat;
import com.gdmob.jweb.beetl.func.EscapeXml;
import com.gdmob.jweb.beetl.func.HasPrivilegeUrl;
import com.gdmob.jweb.beetl.func.OrderBy;
import com.gdmob.jweb.beetl.render.MyBeetlRenderFactory;
import com.gdmob.jweb.beetl.tag.ParamTag;
import com.gdmob.jweb.common.DictKeys;
import com.gdmob.jweb.handler.GlobalHandler;
import com.gdmob.jweb.interceptor.AuthenticationInterceptor;
import com.gdmob.jweb.interceptor.ParamPkgInterceptor;
import com.gdmob.jweb.plugin.ControllerPlugin;
import com.gdmob.jweb.plugin.I18NPlugin;
import com.gdmob.jweb.plugin.PropertiesPlugin;
import com.gdmob.jweb.plugin.SqlXmlPlugin;
import com.gdmob.jweb.plugin.TablePlugin;
import com.gdmob.jweb.thread.ThreadParamInit;
import com.gdmob.jweb.tools.ToolString;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.activerecord.dialect.PostgreSqlDialect;
import com.jfinal.plugin.activerecord.tx.TxByActionMethods;
import com.jfinal.plugin.activerecord.tx.TxByRegex;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;

/**
 * Jfinal API 引导式配置
 */
public class JfinalConfig extends JFinalConfig {
	
	private static Logger log = Logger.getLogger(JfinalConfig.class);
	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me){
		log = Logger.getLogger(JfinalConfig.class);
		//配置常量，beetl视图,错误页面。
		String home = System.getProperty(com.gdmob.jweb.config.Constants.JWEB_HOME,com.gdmob.jweb.config.Constants.DEFAULT_JWEB_HOME_DIR);
		home = (home + File.separator + com.gdmob.jweb.config.Constants.CONFIG_FILE);
		//加载配置文件
		properties = loadPropertyFile(home);
		if(properties==null){
			properties = loadPropertyFile(com.gdmob.jweb.config.Constants.DEFAULT_PROPERTY_PATH);
		}
		//configConstant 缓存 properties
		new PropertiesPlugin(properties).start();
		
		//configConstant 设置字符集
		me.setEncoding(ToolString.encoding); 
		//configConstant 设置是否开发模式
		me.setDevMode(getPropertyToBoolean(DictKeys.config_devMode, false));
		//configConstant 视图Beetl设置
		me.setMainRenderFactory(new MyBeetlRenderFactory());
		
		GroupTemplate groupTemplate = MyBeetlRenderFactory.groupTemplate;
		groupTemplate.registerFunction("hasPrivilegeUrl", new HasPrivilegeUrl());
		groupTemplate.registerFunction("orderBy", new OrderBy());
		groupTemplate.registerFunction("escapeXml", new EscapeXml());
		groupTemplate.registerTag("param", ParamTag.class);
		groupTemplate.registerFormat("dateFormat", new DateFormat());
		
		//configConstant 视图error page设置;
		me.setError401View("/common/401.html");
		me.setError403View("/common/403.html");
		me.setError404View("/common/404.html");
		me.setError500View("/common/500.html");
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) { 
		//configRoute 路由扫描注册
		new ControllerPlugin(me).start();
		
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		//configPlugin 配置Druid数据库连接池连接属性
		DruidPlugin druidPlugin = new DruidPlugin(
				(String)PropertiesPlugin.getParamMapValue(DictKeys.db_connection_jdbcUrl), 
				(String)PropertiesPlugin.getParamMapValue(DictKeys.db_connection_userName), 
				(String)PropertiesPlugin.getParamMapValue(DictKeys.db_connection_passWord), 
				(String)PropertiesPlugin.getParamMapValue(DictKeys.db_connection_driverClass));

		//configPlugin 配置Druid数据库连接池大小
		druidPlugin.set(
				(int)PropertiesPlugin.getParamMapValue(DictKeys.db_initialSize), 
				(int)PropertiesPlugin.getParamMapValue(DictKeys.db_minIdle), 
				(int)PropertiesPlugin.getParamMapValue(DictKeys.db_maxActive));
		druidPlugin.setFilters("stat,wall");
		druidPlugin.setTestOnBorrow(true);
		//configPlugin 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		//arp.setTransactionLevel(4);//事务隔离级别
		arp.setDevMode(getPropertyToBoolean(DictKeys.config_devMode, true)); // 设置开发模式
		arp.setShowSql(getPropertyToBoolean(DictKeys.config_devMode, true)); // 是否显示SQL

		//configPlugin 数据库类型判断
		String db_type = (String) PropertiesPlugin.getParamMapValue(DictKeys.db_type_key);
		if(db_type.equals(DictKeys.db_type_postgresql)){
			//configPlugin 使用数据库类型是 postgresql"
			arp.setDialect(new PostgreSqlDialect());
			
		}else if(db_type.equals(DictKeys.db_type_mysql)){
			//configPlugin 使用数据库类型是 mysql
			arp.setDialect(new MysqlDialect());
			//arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));// 小写
		
		}else if(db_type.equals(DictKeys.db_type_oracle)){
			//configPlugin 使用数据库类型是 oracle
			druidPlugin.setValidationQuery("select 1 FROM DUAL"); //指定连接验证语句(用于保存数据库连接池), 这里不加会报错误:invalid oracle validationQuery. select 1, may should be : select 1 FROM DUAL 
			arp.setDialect(new OracleDialect());
			arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));// 配置属性名(字段名)大小写不敏感容器工厂
		}

		//configPlugin 添加druidPlugin插件
		me.add(druidPlugin);
		
		//configPlugin 表扫描注册
		new TablePlugin(arp).start();
		me.add(arp);

		//I18NPlugin 国际化键值对加载
		me.add(new I18NPlugin());
		
		//EhCachePlugin EhCache缓存
		me.add(new EhCachePlugin());

		//SqlXmlPlugin 解析并缓存 xml sql
		me.add(new SqlXmlPlugin());
	}

	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		//configInterceptor 权限认证拦截器
		me.add(new AuthenticationInterceptor());
		
		//configInterceptor 参数封装拦截器
		me.add(new ParamPkgInterceptor());
		
		//配置开启事物规则
		me.add(new TxByActionMethods("save", "update", "delete"));
		me.add(new TxByRegex(".*save.*"));
		me.add(new TxByRegex(".*update.*"));
		me.add(new TxByRegex(".*delete.*"));
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		//configHandler 全局配置处理器，主要是记录日志和request域值处理
		me.add(new GlobalHandler());
	}
	
	/**
	 * 系统启动完成后执行
	 */
	public void afterJFinalStart() {
		log.info("afterJFinalStart 缓存参数");
		new ThreadParamInit().start();
	}
	
	/**
	 * 系统关闭前调用
	 */
	public void beforeJFinalStop() {

	}
	/**
	 * 运行此 main 方法可以启动项目
	 * 说明：
	 * 1. linux 下非root账户运行端口要>1024
	 * 2. idea 中运行记得加上当前的module名称
	 */
	public static void main(String[] args) {
		JFinal.start("WebContent",80, "/", 5);
	}
}
