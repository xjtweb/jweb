package com.gdmob.jweb.controller;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.model.User;
import com.gdmob.jweb.model.UserInfo;
import com.gdmob.jweb.service.UserService;
import com.gdmob.jweb.validator.UserValidator;
import com.jfinal.aop.Before;

/**
 * 用户管理
 */
@Controller(controllerKey = "/jw/user")
public class UserController extends BaseController {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(UserController.class);
	
	private String groupIds;
	
	/**
	 * 默认列表
	 */
	public void index() {
		UserService.service.list(splitPage);
		render("/pingtai/user/list.html");
	}
	
	/**
	 * 保存
	 */
	@Before(UserValidator.class)
	public void save() {
		String password = getPara("password");
		User user = getModel(User.class);
		UserInfo userInfo = getModel(UserInfo.class);
		UserService.service.save(user, password, userInfo);
		render("/pingtai/user/add.html");
	}
	
	/**
	 * 准备更新
	 */
	public void edit() {
		User user = User.dao.findById(getPara());
		setAttr("user", user);
		setAttr("userInfo", UserInfo.dao.findById(user.getStr("userinfoids")));
		render("/pingtai/user/update.html");
	}
	
	/**
	 * 更新
	 */
	@Before(UserValidator.class)
	public void update() {
		String password = getPara("password");
		User user = getModel(User.class);
		UserInfo userInfo = getModel(UserInfo.class);
		UserService.service.update(user, password, userInfo);
		redirect("/jw/user");
	}

	/**
	 * 查看
	 */
	public void view() {
		User user = User.dao.findById(getPara());
		setAttr("user", user);
		setAttr("userInfo", UserInfo.dao.findById(user.getStr("userinfoids")));
		render("/pingtai/user/view.html");
	}
	
	/**
	 * 删除
	 */
	public void delete() {
		UserService.service.delete(getPara());
		redirect("/jw/user");
	}
	
	/**
	 * 设置用户拥有的组
	 */
	public void setGroup(){
		UserService.service.setGroup(ids, groupIds);
		renderText(ids);
	}
	
	/**
	 * 验证旧密码是否正确
	 */
	public void valiPassWord(){
		String passWord = getPara("passWord");
		boolean bool = UserService.service.valiPassWord(ids, passWord);
		renderText(String.valueOf(bool));
	}
	
	/**
	 * 密码变更
	 */
	public void passChange(){
		String userName = getPara("userName");
		String passOld = getPara("passOld");
		String passNew = getPara("passNew");
		UserService.service.passChange(userName, passOld, passNew);
		renderText("");
	}
	
}


